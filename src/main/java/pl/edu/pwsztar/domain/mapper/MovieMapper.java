package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<Movie,CreateMovieDto>{

    @Override
    public Movie convert(CreateMovieDto movie) {
        return new Movie.Builder().title(movie.getTitle()).videoId(movie.getVideoId()).image(movie.getImage()).year(movie.getYear()).build();
    }
}

