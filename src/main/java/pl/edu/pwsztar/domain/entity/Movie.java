package pl.edu.pwsztar.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;

    @Column(name = "title")
    private String title;

    @Column(name = "image")
    private String image;

    @Column(name = "year")
    private Integer year;

    @Column(name = "video_id")
    private String videoId;

    public Movie() {
    }

    public Movie(Builder builder) {
        title = builder.title;
        image = builder.image;
        year = builder.year;
        videoId = builder.videoId;
        movieId = builder.movieId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public static final class Builder {
        private String title;
        private String image;
        private Integer year;
        private String videoId;
        private Long movieId;

        public Builder() {
        }

        public Movie.Builder title(String title) {
            this.title = title;
            return this;
        }

        public Movie.Builder image(String image) {
            this.image = image;
            return this;
        }

        public Movie.Builder year(Integer year) {
            this.year = year;
            return this;
        }

        public Movie.Builder videoId(String videoId) {
            this.videoId = videoId;
            return this;
        }

        public Movie.Builder movieId(Long movieId) {
            this.movieId = movieId;
            return this;
        }

        public Movie build() {
            return new Movie(this);
        }
    }
}
